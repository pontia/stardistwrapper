# StarDistWrapper

Wrapper around [StarDist](https://github.com/stardist/stardist) to make it easier for end-users.

## Installation

After downloading or cloning the [StarDistWrapper](https://git.bsse.ethz.ch/pontia/stardistwrapper.git) repository, execute the following:

```bash
$ cd stardist_wrapper
$ conda env create -f environment.yml
$ conda activate stardist_wrapper
```

