from datetime import datetime
from typing import Union, Optional, Tuple
import numpy as np
from csbdeep.io import save_tiff_imagej_compatible
from stardist.models import Config3D, StarDist3D
from tifffile import imread, imsave
from pathlib import Path
from stardist import Rays_GoldenSpiral
from tqdm import tqdm

from stardist_wrapper.loaders import TrainerDataManager, SimpleInputDataLoader


class StarDistTrainer3D:

    def __init__(self,
                 input_data_folder: Union[str, Path],
                 label_data_folder: Union[str, Path],
                 *,
                 num_rays: int = 96,
                 num_channel: Optional[int] = None,
                 axes: Optional[str] = None,
                 anisotropy: Optional[list] = None,
                 grid: Optional[list] = None,
                 verbose: Optional[bool] = False,
                 config_custom_settings: Optional[dict] = None
                 ):
        """Class constructor.

        @param input_data_folder: Union[str, Path]
            Folder containing input (raw) images for training. The images must be TIFF files.

        @param label_data_folder: Union[str, Path]
            Folder containing label (ground truth) images for training. The images must be TIFF files.

        @param num_rays: Optional[int], default is 96.
            Number of rays to use to reconstruct the objects; if omitted, it will default to 96.

        @param num_channel: Optional[int]
            Number of input channels; omit to estimate automatically (slower).

        @param num_channel: Optional[int]
            Number of input channels; omit to estimate automatically (slower).

        @param axes: Optional[str]
            Axes geometry; omit to estimate automatically (slower).

        @param anisotropy: Optional[list]
            Median anisotropy of the label objects; omit to estimate automatically.

        @param grid: Optional[list]
            Subsampling grid, to make sure that the median object fits in the network field of view;
            omit to estimate automatically.

        @param verbose: Optional[bool]
            Set to True to get more verbose output from operations; it defaults to False.

        @param config_custom_settings: Optional[dict]
            Any additional parameters to be passed on directly to Config3D() are packed into a dictionary,
            e.g. {'train_learning_rate': 0.001}
        """

        # First store the verbose parameter
        self.verbose = verbose

        # Make sure that config_custom_settings is a dictionary
        if config_custom_settings is None:
            config_custom_settings = {}
        if type(config_custom_settings) is not dict:
            raise Exception("config_custom_settings must be a dictionary!")

        # Store configuration custom settings.
        self.config_custom_settings = config_custom_settings

        # Since initialization may take a few seconds, we inform.
        print(f"Please wait while initializing...")

        # Get a DataManager
        self.data_manager = TrainerDataManager(input_data_folder, label_data_folder, verbose=self.verbose)
        self.data_manager.summary()

        # Prepare the data loaders
        self.X_train, self.Y_train, self.X_valid, self.Y_valid = self.data_manager.get_data_loaders()

        # Store parameters
        self.num_rays = num_rays
        self.anisotropy = anisotropy if anisotropy is not None else self.data_manager.get_anisotropy()
        self.grid = grid if grid is not None else self.data_manager.get_grid()
        self.num_channel = num_channel if num_channel is not None else self.data_manager.get_num_channels()
        self.axes = axes if axes is not None else self.data_manager.get_axes()

        # Store model and model information
        self.model = None
        self.model_base_dir = None
        self.model_name = None

        # Inform
        print(f"Initialization completed.")

    def train(self,
              model_base_dir: Union[str, Path],
              model_name: Optional[str] = None,
              num_rays: Optional[int] = 96,
              grid: Optional[Tuple[int, int, int]] = None,
              anisotropy: Optional[Tuple[float, float, float]] = None,
              train_epochs: Optional[int] = 400,
              train_steps_per_epoch: Optional[int] = 100,
              train_patch_size: Optional[Tuple[int, int, int]] = (24, 32, 32),
              train_batch_size: Optional[int] = 4,
              use_gpu: Optional[bool] = False,
              augmenter: Optional[Tuple] = None,
              optimize_thresholds: Optional[bool] = True
              ):
        """Train.

        @param model_base_dir: Union[str, Path]
            Folder where the StarDist models will be saved.

        @param model_name: Optional[str]
            Name of the model to be saved. If none passed, the name will default to stardist_{current date time}

        @param num_rays: Optional[int], default is 96.
            Number of rays to use to reconstruct the objects; if omitted, it will default to 96.

        @param grid: Optional[Tuple[int, int, int]]
            Subsampling grid (Z, Y, X), to make sure that the median object fits in the network field of view;
            omit to estimate automatically.

        @param anisotropy: Optional[Tuple[float, float, float]]
            Median anisotropy of the label objects (Z, Y, X). if omitted, it will be estimated.

        @param train_epochs: Optional[int]
            Number of training epochs. If omitted, it will default to 400.

        @param train_steps_per_epoch: Optional[int]
            Number of training steps per epoch. If omitted, it will default to 100.

        @param train_patch_size: Optional[Tuple]
            Size of a patch (subvolume) to be used for training. If omitted, it will default to (24, 36, 36).

        @param train_batch_size: Optional[int]
            Number of parches per batch. If omitted, it will default to 4.

        @param use_gpu: Optional[bool]
            Whether to use image preparation operations on GPU (using OpenCL). If omitted, it will default to False.

        @param augmenter: Optional[Tuple]
            Tuple of augmenters to be applied to the training data. If omitted, it will default to StarDist's default
            augmenters.

        @param optimize_thresholds: Optional[bool]
            Run threshold optimization at the end of training. If False, the optimization can still be run with the
            StarDistTrainer3D.optimize_thresholds() method.
        """

        # If no anisotropy is specified, estimate it from the data
        if anisotropy is None:
            anisotropy = self.data_manager.get_anisotropy()

        # If no grid is specified, estimate it from the passed anisotropy or from the data
        if grid is None:
            grid = self.data_manager.get_grid(anisotropy)

        # Create ray spiral
        rays = Rays_GoldenSpiral(num_rays, anisotropy=anisotropy)

        # Create configuration
        conf = Config3D(
            axes=self.axes,
            rays=rays,
            grid=grid,
            anisotropy=anisotropy,
            use_gpu=use_gpu,
            n_channel_in=self.num_channel,
            train_patch_size=train_patch_size,
            train_batch_size=train_batch_size,
            train_epochs=train_epochs,
            train_steps_per_epoch=train_steps_per_epoch,
            **self.config_custom_settings
        )

        # If no model name was specified, we build one
        if model_name is None:
            model_name = f"stardist_{datetime.now().strftime('%Y%m%d_%H%M%S')}"

        # Store the name and dir
        self.model_base_dir = model_base_dir
        self.model_name = model_name

        # Inform
        full_path = (Path(self.model_base_dir) / self.model_name).resolve()
        print(f"Working in {full_path}")

        # Create a model
        self.model = StarDist3D(conf, name=self.model_name, basedir=self.model_base_dir)

        # Display model summary
        self.summary()

        # Check that the field of view is large enough to cover the median size object
        print(f"Comparing field of view to median size object...")
        median_size = self.data_manager.get_extents()
        fov = np.array(self.model._axes_tile_overlap('ZYX'))
        print(f"Current grid         :  {grid}")
        print(f"Median object size   :  {median_size}")
        print(f"Network field of view:  {fov}")
        if any(median_size > fov):
            print("WARNING: median object size larger than field of view of the neural network.")
            print(f"Training will continue: interrupt it to adapt the grid size...")

        # Train
        self.model.train(self.X_train, self.Y_train, validation_data=(self.X_valid, self.Y_valid), augmenter=augmenter)

        # Inform
        print(f"Model saved to {Path(self.model_base_dir) / self.model_name}")

        # Optimize the thresholds
        if optimize_thresholds:

            print(f"Performing threshold optimization: this may take a while...")
            try:
                self.model.optimize_thresholds(self.X_valid, self.Y_valid)
                print(f"Threshold validation completed successfully.")
            except Exception as e:

                print(f"Threshold optimization failed with error: {e}.")
                print(f"Saving validation arrays...")

                # Save the validation data
                val_array_file_name = self.model_base_dir / Path(f"val_arrays_{Path(self.model_name).stem}.npy")
                with open(val_array_file_name, 'wb') as f:
                    np.savez(f, X_val=self.X_valid, Y_val=self.Y_valid)
                print(f"Validation arrays saved to {val_array_file_name}")

    def summary(self):
        """Display model summary."""
        if self.model is None:
            print("Model not instantiated yet.")
            return
        print(self.model.keras_model.summary())

    @staticmethod
    def optimize_thresholds(model_base_dir, model_name):
        """Run threshold optimization.

        @param model_base_dir: Union[str, Path]
            Folder where the StarDist models will be saved.

        @param model_name: str
            Name of the model to be updated.
        """

        # Reload the validation data
        val_array_name = f"{Path(model_name).stem}.npy"
        val_array_full = Path(model_base_dir) / val_array_name
        with open(val_array_full, 'rb') as f:
            data = np.load(f)

        # Optimize the thresholds (and save the update model)
        print(f"Performing threshold optimization: this may take a while...")
        try:

            # Reload the model
            model = StarDist3D(None, name=model_name, basedir=model_base_dir)

            # Optimize thresholds
            model.optimize_thresholds(data["X_val"], data["Y_val"])

            print(f"Threshold validation completed successfully.")
        except Exception as e:

            # Failed: inform.
            print(f"Threshold optimization failed with error: {e}.")

    @staticmethod
    def get_default_augmenter():
        """Return the default StarDist augmenter."""

        def random_fliprot(img, mask, axis=None):
            if axis is None:
                axis = tuple(range(mask.ndim))
            axis = tuple(axis)

            assert img.ndim >= mask.ndim
            perm = tuple(np.random.permutation(axis))
            transpose_axis = np.arange(mask.ndim)
            for a, p in zip(axis, perm):
                transpose_axis[a] = p
            transpose_axis = tuple(transpose_axis)
            img = img.transpose(transpose_axis + tuple(range(mask.ndim, img.ndim)))
            mask = mask.transpose(transpose_axis)
            for ax in axis:
                if np.random.rand() > 0.5:
                    img = np.flip(img, axis=ax)
                    mask = np.flip(mask, axis=ax)
            return img, mask

        def random_intensity_change(img):
            img = img * np.random.uniform(0.6, 2) + np.random.uniform(-0.2, 0.2)
            return img

        def augmenter(x, y):
            """Augmentation of a single input/label image pair.
            x is an input image
            y is the corresponding ground-truth label image
            """
            # Note that we only use fliprots along axis=(1,2), i.e. the yx axis
            # as 3D microscopy acquisitions are usually not axially symmetric
            x, y = random_fliprot(x, y, axis=(1, 2))
            x = random_intensity_change(x)
            return x, y

        return augmenter

    def get_model_info(self):
        """Return the model base_dir and name."""
        return self.model_base_dir, self.model_name


class StarDistPredictor3D:

    def __init__(self,
                 model_base_dir: Union[str, Path],
                 model_name: str,
                 verbose=False
                 ):
        """Class constructor.

        @param model_base_dir: Union[str, Path]
            Folder where the StarDist models will be saved.

        @param model_name: str
            Name of the model to be loaded.

        @param verbose: Optional[bool]
            Set to True to get more verbose output from operations; it defaults to False.
        """

        # First store the verbose parameter
        self.verbose = verbose

        # Model data
        self.model_base_dir = model_base_dir
        self.model_name = model_name

        # Load the model
        self.model = StarDist3D(None, name=self.model_name, basedir=self.model_base_dir)

    def predict(
            self,
            process_folder: Union[str, Path],
            predict_folder: Union[str, Path],
            save_for_imagej: Optional[bool] = False,
            n_tiles: Optional[Tuple[int, int, int]] = (1, 8, 8)
    ):
        """Predict.

        @param process_folder: Union[str, Path]
            Folder containing input (raw) images for prediction. The images must be TIFF files.

        @param predict_folder: Union[str, Path]
            Folder where the predicted labels will be saved.

        @param save_for_imagej: Optional[bool]
            Whether the saved prediction TIFF file should be ImageJ compatible.

        @param n_tiles: Optional[Tuple[int, int, int]]
            Partitioning (Z, Y, X) to use to cover the whole 3D dataset for predictions. The tiles
            will be merged into an output with the same size as the input TIFF file.
        """

        # Get the axes configuration from the model
        axes = self.model.config.axes.replace("C", "")

        # Make sure the prediction folder exists
        predict_folder.mkdir(parents=True, exist_ok=True)

        # Use a SimpleInputDataLoader to get the list of files to process
        data_loader = SimpleInputDataLoader(process_folder)

        # Process
        for i, X in tqdm(enumerate(data_loader)):

            # Predict
            predicted, _ = self.model.predict_instances(X, axes=axes, n_tiles=n_tiles)

            # Save the prediction
            if save_for_imagej:
                out_filename = predict_folder / Path("ij_predict_" + Path(data_loader.get_file_name(i)).name)
                save_tiff_imagej_compatible(str(out_filename), predicted, axes=axes)
            else:
                out_filename = predict_folder / Path("predict_" + Path(data_loader.get_file_name(i)).name)
                imsave(str(out_filename), predicted)

            del X, predicted
