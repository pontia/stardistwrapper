import glob
from pathlib import Path
from natsort import natsorted
import numpy as np
from stardist import fill_label_holes, calculate_extents
from csbdeep.utils import normalize
from tensorflow.keras.utils import Sequence
from tifffile import imread
from functools import lru_cache


class SimpleInputDataLoader(Sequence):
    """Iterates over the passed input data."""

    def __init__(self, input_files, *, p_min=1, p_max=99.8, verbose=False):
        """Constructor."""
        super().__init__()

        # Store the normalization percentiles
        self.p_min = p_min
        self.p_max = p_max

        # Store the file list
        self.input_files = input_files

        # Store the number of files
        self.num_files = len(self.input_files)

        # Verbose
        self.verbose = verbose

    def __len__(self):
        """Return the len of the object."""
        return self.num_files

    @lru_cache(10)
    def __getitem__(self, index):
        """Access by index."""

        # Load
        X = imread(self.input_files[index])

        # Normalize axes jointly
        X = normalize(X, self.p_min, self.p_max, axis=None)

        # Return the dataset
        return X

    def get_file_name(self, index):
        """Return the file name at given index."""
        return self.input_files[index]


class SimpleLabelDataLoader(Sequence):
    """Iterates over the passed label data."""

    def __init__(self, label_files, *, verbose=False):
        """Constructor."""
        super().__init__()

        # Store the file list
        self.label_files = label_files

        # Store the number of files
        self.num_files = len(self.label_files)

        # Cache the data
        self._data_cache = self.num_files * [None]

        # Verbose
        self.verbose = verbose

    def __len__(self):
        """Return the len of the object."""
        return self.num_files

    @lru_cache(10)
    def __getitem__(self, index):
        """Access by index."""

        # Load
        Y = imread(self.label_files[index])

        # Make sure the label dataset is of dtype np.uint16
        if Y.dtype != np.uint16:
            Y = Y.astype(np.uint16)

        # Fill holes in labels
        Y = fill_label_holes(Y)

        # Return the dataset
        return Y


class TrainerDataManager:
    """Simple data loader to pass to the StarDist model."""

    def __init__(self, input_dir, label_dir, *, val_fraction=0.15, seed=42, verbose=False):
        """Constructor."""

        # Random number generation initialization
        self.rng = np.random.RandomState(seed)

        # Store the verbosity
        self.verbose = verbose

        # Initialize counter
        self.n = 0

        # Check train_dir for validity and scan it
        self.input_dir = Path(input_dir).resolve()

        # Cache calculated values
        self._calculated_extents = None
        self._calculated_anisotropy = None

        if not self.input_dir.is_dir():
            raise NotADirectoryError(f"{input_dir} is not a valid directory.")
        self.input_files = natsorted(glob.glob(str(self.input_dir) + "/*.tif*"))
        num_input_files = len(self.input_files)
        if num_input_files == 0:
            raise Exception("No input files found.")

        # Check train_dir for validity and scan it
        self.label_dir = Path(label_dir).resolve()
        if not self.label_dir.is_dir():
            raise NotADirectoryError(f"{label_dir} is not a valid directory.")
        self.label_files = natsorted(glob.glob(str(self.label_dir) + "/*.tif*"))
        num_label_files = len(self.label_files)
        if num_label_files == 0:
            raise Exception("No label files found.")

        # Do we have the same number of files?
        if num_label_files != num_input_files:
            raise Exception("The number of input and label files differ.")

        # Make sure all file names match
        if not all(Path(r).name == Path(g).name for r, g in zip(self.input_files, self.label_files)):
            raise Exception("The names of input and label files do not match.")

        # Store the permuted indices
        indices = self.rng.permutation(num_input_files)

        # Split them between training and validation
        n_valid = max(1, int(round(val_fraction * len(indices))))
        self.train_indices = indices[:-n_valid]
        self.valid_indices = indices[-n_valid:]

        self.train_inputs = [self.input_files[i] for i in self.train_indices]
        self.train_labels = [self.label_files[i] for i in self.train_indices]
        self.valid_inputs = [self.input_files[i] for i in self.valid_indices]
        self.valid_labels = [self.label_files[i] for i in self.valid_indices]

        # Create the training and validation SimpleDataLoaders
        self.trainInputDataLoader = SimpleInputDataLoader(self.train_inputs, verbose=self.verbose)
        self.trainLabelDataLoader = SimpleLabelDataLoader(self.train_labels, verbose=self.verbose)
        self.validInputDataLoader = SimpleInputDataLoader(self.valid_inputs, verbose=self.verbose)
        self.validLabelDataLoader = SimpleLabelDataLoader(self.valid_labels, verbose=self.verbose)

    def get_extents(self, force=False):
        """Return the median extents of the labels from the first label file."""
        if not force and self._calculated_extents is not None:
            return self._calculated_extents
        self._calculated_extents = calculate_extents(self.trainLabelDataLoader[0], np.median)
        return self._calculated_extents

    def get_anisotropy(self, force=False):
        """Return the anisotropy of the labels from the first label file."""
        if not force and self._calculated_anisotropy is not None and self._calculated_extents is not None:
            return self._calculated_anisotropy

        self.get_extents(force=force)
        print(f"Calculating anisotropy of labeled objects...")
        self._calculated_anisotropy = tuple(np.max(self._calculated_extents) / self._calculated_extents)
        print(f"Empirical anisotropy of labeled objects: ("
              f"{self._calculated_anisotropy[0]:.2f}, "
              f"{self._calculated_anisotropy[1]:.2f}, "
              f"{self._calculated_anisotropy[2]:.2f})"
              )
        return self._calculated_anisotropy

    def get_axes(self):
        """Return the axis geometry from the first input file."""
        return "ZYX"

    def get_data_loaders(self):
        """Return the training and validation loaders."""
        return self.trainInputDataLoader, self.trainLabelDataLoader, self.validInputDataLoader, self.validLabelDataLoader

    def get_grid(self, anisotropy=None):
        """Return a suggested grid."""
        if anisotropy is None:
            anisotropy = self.get_anisotropy()
        grid = tuple(1 if a > 1.5 else 2 for a in anisotropy)
        print(f"Suggested grid size: {grid}")
        return grid

    def get_num_channels(self):
        """Return the number of channels of the input data."""
        X = self.trainInputDataLoader[0]
        return 1 if X.ndim == 3 else X.shape[-1]

    def summary(self):
        """Show the result of the processing."""
        print(f"Using {len(self.train_inputs)} training and {len(self.valid_inputs)} validation file(s).")

        if self.verbose:
            print(f"Training set ({len(self.train_inputs)}):")
            for x, y in zip(self.train_inputs, self.train_labels):
                print(f"X = {Path(x).name}, Y= {Path(y).name}")
            print(f"\nValidation set ({len(self.valid_inputs)}):")
            for x, y in zip(self.valid_inputs, self.valid_labels):
                print(f"X = {Path(x).name}, Y= {Path(y).name}")
