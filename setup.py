from setuptools import setup
from stardist_wrapper import __version__

setup(name='stardist_wrapper',
      version=__version__,
      author='Aaron Ponti',
      author_email='aaron.ponti@bsse.ethz.ch',
      url='https://git.bsse.ethz.ch/pontia/stardistwrapper',
      description='Simple wrapper around StarDist.',
      long_description='',
      include_package_data=True,
      packages=['stardist_wrapper'],
      package_dir={'stardist_wrapper': 'stardist_wrapper'},
      provides=['stardist_wrapper'],
      keywords='StarDist',
      license='GPL2.0',
      classifiers=['Development Status :: 2 - Pre-Alpha',
                   'Intended Audience :: Developers',
                   'Natural Language :: English',
                   'Operating System :: POSIX :: Linux',
                   'Programming Language :: Python :: 3',
                   'License :: OSI Approved :: Apache Software License',
                   'Topic :: Scientific/Engineering'
                  ],
      requires=['numpy', 'cudatoolkit', 'cudnn', 'tensorflow', 'natsort', 'jupyter', 'gputools', 'edt', 'stardist']
)
