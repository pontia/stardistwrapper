import sys
from stardist_wrapper import StarDistTrainer3D, StarDistPredictor3D

INPUT_DATA_FOLDER = "Data/Inputs/"
LABEL_DATA_FOLDER = "Data/Labels/"
PROCESS_DATA_FOLDER = "Data/Process/"
PREDICT_DATA_FOLDER = "Data/Predict/"

if __name__ == "__main__":

    # Initialize the Trainer
    w = StarDistTrainer3D(INPUT_DATA_FOLDER, LABEL_DATA_FOLDER, verbose=True)

    # Train
    w.train(model_base_dir="models", grid=(1, 8, 8), train_patch_size=(48, 64, 64), augmenter=w.get_default_augmenter())

    # Get the model name
    model_base_dir, model_name = w.get_model_info()

    # Initialize the Predictor
    p = StarDistPredictor3D(model_base_dir, model_name)

    # Predict
    p.predict(PROCESS_DATA_FOLDER, PREDICT_DATA_FOLDER, n_tiles=(1, 8, 8))

    sys.exit(0)